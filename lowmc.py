"""
    Polynomial system generator for LowMC block cipher [ARSTZ15].

    AUTHOR : Rusydi H. Makarim <makarim@cwi.nl, r.h.makarim@math.leidenuniv.nl>

    REFERENCES:
    [ARSTZ15] M. Albrecht, C. Rechberger, T. Schneider, T. Tiessen, M. Zohner;
        *Ciphers for MPC and FHE*; EUROCRYPT 2015; Available at
        https://eprint.iacr.org/2016/687
"""

from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator
from sage.crypto.sbox import SBox
from sage.groups.matrix_gps.linear import GL
from sage.matrix.matrix_space import MatrixSpace
from sage.modules.free_module import VectorSpace
from sage.rings.finite_rings.finite_field_constructor import GF


class LowMC(MPolynomialSystemGenerator):

    def __init__(self, n=128, m=31, k=80, r=12, **kwargs):
        """
            INPUT:
                n -- block size
                k -- key size
                m -- no. of SBoxes
                r -- no. of rounds
                direct_repr -- whether to use direct representation of the SBox (default : False)
                use_polybori -- whether to use BooleanPolynomialRing (default : True)
                order -- monomial order (default : deglex)

            OUTPUT:
                LowMC class

            RECOMMENDED PARAMETERS:

                -------------------------------
                |   n  |  m |  k  |  d  |  r  |
                -------------------------------
                | 256  | 49 | 80  | 64  | 12  |
                | 128  | 31 | 80  | 64  | 12  |
                | 64   | 1  | 80  | 64  | 164 |
                | 1024 | 20 | 80  | 64  | 45  |
                | 1024 | 10 | 80  | 64  | 85  |
                ------------------------------
                | 256  | 63 | 128 | 128 | 14  |
                | 196  | 63 | 128 | 128 | 14  |
                | 128  | 3  | 128 | 128 | 88  |
                | 128  | 2  | 128 | 128 | 128 |
                | 128  | 1  | 128 | 128 | 252 |
                | 1024 | 20 | 128 | 128 | 49  |
                | 1024 | 10 | 128 | 128 | 92  |
                -------------------------------
                | 512  | 66 | 256 | 256 | 18  |
                | 256  | 10 | 256 | 256 | 52  |
                | 256  | 1  | 256 | 256 | 458 |
                | 1024 | 10 | 256 | 256 | 103 |
                -------------------------------

            EXAMPLE:
                sage: load("LowMC.py")
                sage: LMC = LowMC(n = 16, m = 3, k = 10, r = 2, use_polybori=False)
                sage: F, s = LMC.polynomial_system()
                sage: F
                Polynomial Sequence with 164 Polynomials in 92 Variables
                sage: G = F.groebner_basis()
                sage: G[:10]
                [K00 + 1, K01 + 1, K02, K03, K04 + 1, K05 + 1, K06, K07 + 1, K08 + 1, K09]
                sage: s
                {K09: 0,
                 K08: 1,
                 K07: 1,
                 K06: 0,
                 K05: 1,
                 K04: 1,
                 K03: 0,
                 K02: 0,
                 K01: 1,
                 K00: 1}
        """

        self.n = n
        self.m = m
        self.k = k
        self.r = r
        self._sbox = SBox([0, 1, 3, 6, 7, 4, 5, 2], big_endian=False)
        self._direct_repr = kwargs.get("direct_repr", False)
        self._use_polybori = kwargs.get("use_polybori", True)
        self._order = kwargs.get("order", "deglex")
        self._base = GF(2)

        GLn = GL(n, self._base)
        self.LMatrix = [None] + [GLn.random_element().matrix()
                                 for _ in range(r)]

        VS = VectorSpace(self._base, n)
        self.Constants = [None] + [VS.random_element() for _ in range(r)]

        self.KMatrix = []
        MS = MatrixSpace(self._base, n, k)
        rank = min(n, k)
        for i in range(r + 1):
            M = None
            while(True):
                M = MS.random_element()
                if M.rank() == rank:
                    break
            self.KMatrix.append(M)

    def varformatstr(self, name):
        fmt = None
        n, r, k = self.n, self.r, self.k
        l = str(max([len(str(n)), len(str(r)), len(str(k))]))

        if name.startswith("K"):
            fmt = name + "%0" + l + "d"
        else:
            fmt = name + "%0" + l + "d" + "%0" + l + "d"

        return fmt

    def varstrs(self, name, round):
        s = self.varformatstr(name)
        ls = []

        if name.startswith("K"):
            k = self.k
            ls = [s % (i) for i in range(k)]
        elif name.startswith("Y"):
            m = self.m
            ls = [s % (round, i) for i in range(3 * m)]
        else:
            n = self.n
            ls = [s % (round, i) for i in range(n)]

        return ls

    def vars(self, name, round=None):
        return [self.R(e) for e in self.varstrs(name, round)]

    def ring(self, order=None):
        """
            Return a polynomial ring over F_2 that holds all variables in a LowMC instance.
            If self._use_polybori is True, we use PolyBori library. Otherwise, we use
            Singular.

            The variable names correspond to the following :

                K{j} - Key variables (0 <= j <= k-1)
                X{i}{j} - Input to the round i (1 <= i <= r ; 0 <= j <= n-1)
                Y{i}{j} - Output of the S-Box layer in round i (1 <= i <= r ; 0 <= j <= 3m-1)

            INPUT:
                order -- monomial order
        """
        if order is None:
            order = self._order

        n = self.n
        m = self.m
        r = self.r
        k = self.k

        var_names = []
        for i in range(1, r + 1):
            var_names += [self.varformatstr("X") % (i, j) for j in range(n)]
            var_names += [self.varformatstr("Y") % (i, j)
                          for j in range(3 * m)]
        var_names += [self.varformatstr("K") % (j) for j in range(k)]

        if self._use_polybori:
            R = BooleanPolynomialRing(len(var_names), var_names, order=order)
        else:
            R = PolynomialRing(GF(2), len(var_names), var_names, order=order)
        self.R = R

        return R

    def sbox(self):
        """
            Return the instance mq.SBox for LowMC S-Box
        """
        return self._sbox

    def single_sbox_polynomials(self, x, y):
        """
            Return a set of polynomials representing the S-Box. If self._direct_repr
            is True, it uses algebraic normal form of the coordinate functions.

            INPUT:
                x -- input variables
                y -- output variables
        """
        l = []

        x0, x1, x2 = x[0], x[1], x[2]
        y0, y1, y2 = y[0], y[1], y[2]

        if self._direct_repr:
            l = [y0 + x0 + x1 * x2 + x1 + x2,
                 y1 + x0 * x2 + x1 + x2,
                 y2 + x0 * x1 + x2]
        else:
            l = [x0 * x2 + x1 + x2 + y1,
                 x0 * x1 + x2 + y2,
                 x0 * y1 + x2 + y2,
                 x0 * y2 + x1 + y1 + y2,
                 x1 * x2 + x0 + x1 + x2 + y0,
                 x1 * y0 + x1 + x2 + y2,
                 x0 * y0 + x1 * y1 + x1 + x2 + y0 + y1 + y2,
                 x1 * y2 + x0 + x1 + y0 + y2,
                 x2 * y0 + x1 + y1,
                 x2 * y1 + x0 + x2 + y0 + y1,
                 x0 * y0 + x2 * y2 + x0 + x1 + x2 + y1 + y2,
                 y0 * y1 + x2 + y1 + y2,
                 y0 * y2 + x1 + y1,
                 y1 * y2 + x0 + y0 + y1 + y2]
        return l

    def polynomial_system(self, P=None, K=None, **kwds):
        """
            Return a polynomial system for the LowMC instance.

            INPUT:
                P -- plaintext
                K -- key

            OUTPUT:
                F -- PolynomialSequence
                s -- A python dictionary that maps the key variables to its value.
                     It can be used to verify if the solution for an attack is correct.
        """
        n = self.n
        k = self.k
        r = self.r
        m = self.m

        if P is None:
            P = self.random_element(n)
        if K is None:
            K = self.random_element(k)
        C = self(P, K)

        F = []

        X1 = self.vars("X", 1)
        F += self.key_addition_polynomials(P, X1, 0)

        for i in range(1, r + 1):
            Xi = self.vars("X", i)
            Yi = self.vars("Y", i)

            F += self.sbox_layer_polynomials(Xi[:3 * m], Yi)
            AL = self.affine_layer_polynomials(Yi + Xi[3 * m:], i)

            if (i < r):
                Xii = self.vars("X", i + 1)
                F += self.key_addition_polynomials(AL, Xii, i)
            else:
                F += self.key_addition_polynomials(AL, C, i)

        s = dict(zip(self.vars("K"), K))
        return Sequence(F), s

    def key_addition_polynomials(self, X, Y, r):
        """
            Return the polynomial representation of the key addition layer.

            INPUT:
                X -- input variables
                Y -- output variables
                r -- round index
        """
        Kvars = vector(self.vars("K"))
        K = self.KMatrix[r] * Kvars
        return [X[i] + K[i] + Y[i] for i in range(self.n)]

    def affine_layer_polynomials(self, X, r):
        """
            Return the polynomial representation of the affine layer.

            INPUT:
                X -- input variables
                r -- round index
        """
        Lr = self.LMatrix[r]
        Cr = self.Constants[r]
        return list(Lr * vector(X) + Cr)

    def sbox_layer_polynomials(self, X, Y):
        """
            Return the polynomial representation of the SBox layer.

            INPUT :
                X -- input variables
                Y -- output variables
        """
        s = 3
        return sum([self.single_sbox_polynomials(X[j:j + s], Y[j:j + s])
                    for j in range(0, self.m * s, s)], [])

    def __call__(self, P, K):
        """
            Encryption of plaintext P using key K

            INPUT:
                P -- plaintext
                K -- key

            OUTPUT:
                The corresponding ciphertext C
        """
        base_field = self._base
        LMatrix = self.LMatrix
        Constants = self.Constants
        KMatrix = self.KMatrix
        S = self.sbox()
        m = self.m
        n = self.n
        r = self.r

        if isinstance(P, list):
            P = vector(base_field, P)
        if isinstance(K, list):
            K = vector(base_field, K)

        def SBoxlayer(X):
            Y = sum([S(X[i:i + 3]) for i in range(0, 3 * m, 3)], [])
            if n > 3 * m:
                Y += list(X[3 * m:])

            return vector(base_field, Y)

        # initial whitening
        state = P + KMatrix[0] * K

        for i in range(1, r + 1):
            # substitution on the the first 3*m bit state
            state = SBoxlayer(state)

            # affine layer
            state = LMatrix[i] * state
            state = state + Constants[i]

            # generate round key and add to the state
            state = state + KMatrix[i] * K

        return state

    def random_element(self, N):
        """
            Return a random binary vector of length N

            INPUT:
                N -- length of the binary vector
        """
        return [self._base.random_element() for _ in range(N)]
